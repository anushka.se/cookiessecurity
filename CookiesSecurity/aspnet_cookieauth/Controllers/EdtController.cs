﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace aspnet_cookieauth.Controllers
{
    public class EdtController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult Transfer()
        {
            ViewBag.IsTransfered = false;
            return View(new TransModel());
        }

        [HttpPost]
        [Authorize]
        public IActionResult Transfer(TransModel model)
        {

            ViewBag.IsTransfered = true;
            return View();
        }
    }

    public class TransModel
    {
        public int amount { get; set; }
    }
}
